#include <data_structures.h>
#include <cstring>

#include <iostream>
int main()
{
    fenwick_tree<int> v;

    for(int i = 1; i <= 100; i++) v.push_back(i);

    std::cout << v.prefix_sum(99) << std::endl;

    directed_graph<long long, long long> graph;
    graph.floyd_warshall();
    std::string s;
    while(true)
    {
        std::cout << "enter 'e' to insert an edge\nenter 'd' to get the distance beetween to vertices\nanything else to quit" << std::endl;
        std::cin >> s;
        if(strcmp(s.c_str(),"e") == 0)
        {
            int x,y,w;
            std::cout << "enter three numbers\nsource dest weight" << std::endl;
            std::cin >> x >> y >> w;
            graph.add_edge(x, y, w);
        }
        else if(strcmp(s.c_str(),"d") == 0)
        {
            int x, y;
            std::cout << "enter two numbers\nsource dest" << std::endl;
            std::cin >> x >> y;
            std::cout << "the distance is: "<< graph.dijkstra(x)[y] << std::endl;
        }
        else break; 
    }
    return 0;
}
