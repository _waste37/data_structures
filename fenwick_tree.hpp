#ifndef _FENWICK_TREE
#define _FENWICK_TREE

#include <vector>

template <typename number, typename =
typename std::enable_if<std::is_arithmetic<number>::value, number>::type>

class fenwick_tree
{
    #define lsb(i) ((i) & (-(i)))
    #define is_pow2(i) (((i) & ((i) - 1)) == 0)
    using index = unsigned int;
    using size = unsigned long;

public:
    fenwick_tree() : sz(0) { v.resize(3); }
    fenwick_tree(std::vector<number> s) : v(s), sz(s.size())
    { init_tree(s); }

    number get(index i) { return range_sum(i, i + 1); }
    void set(index i, number n) { increase(i, n - get(i)); }
    size get_size() { return sz; }

    number prefix_sum(index i)
    {
        number sum = v[0];
	    while (i != 0)
		{ sum += v[i]; i -= lsb(i); }
	    return sum;
    }

    void push_back(number n)
    {
        if(sz == v.size()) v.resize(next_size());
        set(sz, n);
        sz++;
    }

    number range_sum(index i, index j)
    {
	    number sum = 0;
	    for (; j > i; j -= lsb(j)) sum += v[j];
	    for (; i > j; i -= lsb(i)) sum -= v[i];
	    return sum;
    }

    void increase(index i, number delta)
    {
        if (i == 0) { v[0] += delta; return; }
	    for (; i < v.size(); i+= lsb(i)) v[i] += delta;
    }

    std::vector<number> to_vector()
    {
        std::vector<number> A(v);
	    for (index i = v.size() - 1; i > 0; i--)
        {
		    index j = i + lsb(i);
		    if (j < v.size())
			    A[j] -= A[i];
	    }
        return A;
    }

    //only for positive integers, get the max i for prefix sum <= value
    index rank_query(number value)
    {
        index i = 0, j = v.size() - 1;
        value -= v[0];
        for (; j > 0;  j >>= 1) {
            if (i + j < v.size() && v[i + j] <= value) {
                value -= v[i + j];
                i += j;
            }
        }
	    return i;
    }

private:
    void init_tree(std::vector<number> s)
    {
        if(sz == 0) return;
        if(!is_pow2(sz-1))
        {
            size true_sz = sz;
            for(index i = 0; i < 6; i++)
                true_sz |= true_sz >> (1 << i);
            true_sz += 2;
            v.resize(true_sz);
        }

        for(index i = 1; i < v.size(); ++i)
        {
            index j = i + lsb(i);
            if (j < v.size())
            v[j] += v[i];
        }
    }

    size next_size() { return ((v.size() - 1) << 1) + 1; }
    std::vector<number> v;
    size sz;
    index shift;
    #undef lsb
    #undef is_pow2
};

#endif //_FENWICK_TREE
