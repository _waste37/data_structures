#ifndef WASTE_BINARY_HEAP
#define WASTE_BINARY_HEAP

#include <vector>
#include <iostream>
#define _hparnt(x) ( (x & 1) == 0 ? (x / 2) - 1 : x / 2 )
#define _hl_son(x) (2*x + 1)
#define _hr_son(x) (2*x + 2)

typedef enum { MIN_HEAP, MAX_HEAP } heap_type;

template <typename K, typename V>
class binary_heap
{
public:
    binary_heap(heap_type type = heap_type::MIN_HEAP) { this->type = type;}

    binary_heap(std::vector<std::pair<K, V>> &v, heap_type type = heap_type::MIN_HEAP) // O(n)
    {
	this->type = type;
	std::copy(v.begin(), v.end(), std::back_inserter(heap));
	for(int i = heap.size()/2; i >= 0; i--)
	    fix_heap(i, this->heap);
    }

    void insert(K key, V value)
    {
	heap.push_back(std::make_pair(key, value));
	int tmp = heap.size() - 1;
	while(tmp > 0 && heap_comp(heap[tmp].first, heap[tmp-1].first) >= 0)
	    move_up(tmp);
    }

    unsigned long size() { return heap.size(); }

    V find_m() { return heap[0].second; }

    V delete_m()
    {
	V result = heap[0].second;
	heap[0] = heap[heap.size() - 1];
	heap.resize(heap.size() - 1);
	fix_heap(0, this->heap);
	return result;
    }

    void print()
    {
	std::cout << "{";
	for(unsigned int i = 0; i < heap.size(); i++)
	    std::cout << heap[i].first << ", ";
	std::cout << "\b\b}\n";
    }

    void heap_sort(std::vector<std::pair<K,V>> &v)
    {
	unsigned int size = v.size();
	for(int i = size/2; i >= 0; i--)
	    fix_heap(i, v);

	while(size > 1)
	{
	    std::pair<K, V> tmp = v[0]; // estrazione del massimo
	    v[0] = v[size-1];
	    v[size - 1] = tmp;
	    fix_heap(0, v, --size); // ristoro l'albero
	}
    }
private:
    void fix_heap(unsigned int node, std::vector<std::pair<K, V>> &heap, unsigned int size = 0) // O(log n)
    {
	if(size == 0) size = heap.size();
	std::pair<K, V> tmp;
	unsigned int u, l, r;
	while(_hl_son(node) < size)
	{

	    l = _hl_son(node);
	    r = _hr_son(node);
	    if(r < size)
		u = heap_comp(heap[l].first, heap[r].first) > 0 ? l : r;
	    else u = l;

	    if(heap_comp(heap[node].first, heap[u].first) >= 0) break;
	    tmp = heap[node];
	    heap[node] = heap[u];
	    heap[u] = tmp;
	    node = u;
	}
    }

    void move_up(unsigned int node)
    {
	std::pair<K,V> p = heap[node];
	heap[node] = heap[_hparnt(node)];
	heap[_hparnt(node)] = p;
    }

    int heap_comp(K a, K b)
    {
	if(type == heap_type::MAX_HEAP)
	{
	    if(a > b) return 1;
	    else if(a == b) return 0;
	    else return -1;
	}
	else
	{
	    if(a < b) return 1;
	    else if(a == b) return 0;
	    else return -1;
	}
    }
    std::vector<std::pair<K, V>> heap;
    heap_type type;
};

#undef _hparnt
#undef _hl_son
#undef _hr_son

#endif
