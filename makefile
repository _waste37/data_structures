CPP = g++
INCLUDE= -I./
CPPFLAGS= -Wall -Wextra -pedantic -g
HEADERS = $(shell find -name '*.hpp')

error_check : error_check.cpp $(HEADERS)
	$(CPP) error_check.cpp -o error_check $(CPPFLAGS) $(INCLUDE) 

.PHONY: 
	clean

clean:
	rm -f *.o error_check
