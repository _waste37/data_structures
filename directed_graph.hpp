#ifndef WASTE_DIRECTED_GRAPH
#define WASTE_DIRECTED_GRAPH

#include <def.h>
#include <algorithm>
#include <unordered_map>
#include <list>
#include <queue>
#include <limits>
#include <stack>
#include <sstream>
#include <union_find.hpp>

template <typename v_type,
		  typename e_type,
		  typename = typename std::enable_if<
	  	  std::is_arithmetic<e_type>::value, e_type
	  	  >::type>
class directed_graph
{
public:
    directed_graph() : e(0) {}

    std::list<std::pair<v_type, e_type>> neighbours(v_type v) { return adj_map[v]; }

    void add_vertex(v_type v) { adj_map[v]; } //O(1)

    void add_edge(v_type x, v_type y, e_type weight) //O(1)
    {
		adj_map[x].push_back(std::make_pair(y, weight));
		adj_map[y];	e++;
    }

	int num_edges() { return e; } //O(1)
    int num_vertices() { return adj_map.size(); } //O(1)

	std::string to_string(v_type v, graph_visit type = graph_visit::BFS)
    {
		std::string result;
		std::stringstream ss;
		result.append("[");
		for(typename directed_graph<v_type, e_type>::iterator
			it = begin(v, type); it != end(); it++)
				ss << *it << ", ";
		result.append(ss.str());
		result.append("\b\b]");
		return result;
    }

    std::unordered_map<v_type, e_type> dijkstra(v_type v) //O(m log n) CON MODIFICHE DI GIORDANO COLLI
    {
		std::priority_queue<std::pair<v_type, e_type>,
				    std::vector<std::pair<v_type, e_type>>,
				    compare_edges> queue;
		
		std::unordered_map<v_type, bool> enqueued;
		std::unordered_map<v_type, e_type> dist;
		
        for(const typename std::pair<v_type, std::list<std::pair<v_type, e_type>>> it : adj_map)
		    dist[it.first] = POSITIVE_INFINITY;

		dist[v] = 0;
		queue.push(std::make_pair(v, 0));
        enqueued[v] = true;

		while(!queue.empty())
		{
		    std::pair<v_type, e_type> tmp = queue.top();
		    queue.pop();
            enqueued[tmp.first] = false;

		    for(const typename std::pair<v_type, e_type> &x : adj_map[tmp.first])
		    {
				if(dist[x.first] > dist[tmp.first] + x.second)
				{
                    if(!enqueued[x.first])
                    {
				        queue.push(std::make_pair(x.first, dist[x.first]));
                        enqueued[x.first] = true;
                    }
                    dist[x.first] = dist[tmp.first] + x.second;
				}
		    }
		}
		return dist;
	}

    std::unordered_map<v_type, std::unordered_map<v_type, e_type>> floyd_warshall()
    {
        unsigned long i = 0;
        std::unordered_map<int, v_type> _id;
        for(typename std::pair<v_type, std::list<std::pair<v_type, e_type>>> x : adj_map)
                _id[i++] = x.first;

        std::unordered_map<v_type, std::unordered_map<v_type, e_type>> d;

        for(const typename std::pair<v_type, std::list<std::pair<v_type, e_type>>> x : adj_map)
			for(const typename std::pair<v_type, std::list<std::pair<v_type, e_type>>> y : adj_map)
			{
				if(x.first == y.first) d[x.first][y.first] = 0;
				else
				{ 
					d[x.first][y.first] = POSITIVE_INFINITY;
					for(const typename std::pair<v_type, e_type> &it : x.second) 
						if(it.first == y.first) 
						{ d[x.first][y.first] = it.second; break; } 
				}
			}

        for(unsigned long k = 0; k < adj_map.size(); k++)
        {
            for(const typename std::pair<v_type, std::list<std::pair<v_type, e_type>>> x : adj_map)
                for(const typename std::pair<v_type, std::list<std::pair<v_type, e_type>>> y : adj_map)
                    if(d[x.first][_id[k]] != POSITIVE_INFINITY && d[_id[k]][y.first] != POSITIVE_INFINITY &&
					   d[x.first][_id[k]] + d[_id[k]][y.first] < d[x.first][y.first])
					{ d[x.first][y.first] = d[x.first][_id[k]] + d[_id[k]][y.first]; }
        }
        return d;
    }

	directed_graph<v_type, e_type> kruskal()
	{
		union_find<v_type> forest;
		directed_graph<v_type, e_type> G;
		std::vector<std::tuple<v_type, e_type, v_type>> edges;

		for(const typename std::pair<v_type, std::list<std::pair<v_type, e_type>>> it : adj_map)
		{
			forest.make_set(it.first);
			for(const typename std::pair<v_type, e_type> itr : it.second)
				edges.push_back(std::make_tuple(it.first, itr.second, itr.first));
		}

		std::sort(edges.begin(), edges.end(), 
		[](std::tuple<v_type, e_type, v_type> a, std::tuple<v_type, e_type, v_type> b)
		{ return std::get<1>(a) < std::get<1>(b); });

		for(const typename std::tuple<v_type, e_type, v_type> &it : edges)
		{
			v_type x = forest.find(std::get<0>(it));
			v_type y = forest.find(std::get<2>(it));
			if(x != y)
			{
				forest.union_set(x, y);
				G.add_edge(x, y, std::get<1>(it));
			}
		}
		return G;
	}

//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	

	typedef struct iterator
    {
	    using iterator_category = std::forward_iterator_tag;
	    using difference_type   = std::ptrdiff_t;
	    using value_type        = v_type;
	    using pointer           = v_type*;
	    using reference         = v_type&;

	    iterator(graph_visit t, pointer source, directed_graph *owner) 
		: type(t), owner(owner) { start(source); }
		
		reference operator*() const { return *ptr; }
	    pointer operator->() { return ptr; }

	    iterator& operator++() { step(); return *this; }
	    iterator operator++(int) { iterator tmp = *this; step(); return tmp; }

	    friend bool operator== (const iterator& a, const iterator& b) { return a.ptr == b.ptr; };
	    friend bool operator!= (const iterator& a, const iterator& b) { return a.ptr != b.ptr; };

	private:
		void start(pointer source)
		{
			ptr = source;
			if(ptr == 0) return;
			if(type == graph_visit::BFS) 
			{
				q = new std::queue<value_type>();
				q->push(*source);
				bfs_step();
			}
			else
			{
				st = new std::stack<value_type>();
				st->push(*source);
				dfs_step();
			}
		}

		void step() 
		{
			if(ptr == 0) return;
			if(type == graph_visit::BFS) bfs_step(); 
			else dfs_step();
		}
		
		void bfs_step()
		{
			if(q->empty()) { ptr = 0; return; }
			std::list<std::pair<value_type, e_type>> n;
	    		
			while(visited[q->front()] == 2) q->pop();
			cur = q->front();
	    	visited[cur] = 2;
			q->pop();

	    	n = owner->neighbours(cur);
	    	for(const typename std::pair<v_type, e_type> &it : n)
	    	{
				if(visited[it.first] == 0)
				{
			    	q->push(it.first);
		    		visited[it.first] = 1;
				}
	    	}
			ptr = &cur;
		}

		void dfs_step()
		{
			while(visited[st->top()] == 2) st->pop();
			if(st->empty()) { ptr = 0; return; }
			
			std::list<std::pair<value_type, e_type>> n;
			cur = st->top();
	    	visited[cur] = 2;
			st->pop();

	    	n = owner->neighbours(cur);
	    	for(const typename std::list<std::pair<v_type, e_type>> &it : n)
	    	{
				if(visited[it.first] == 0)
				{
			    	st->push(it.first);
		    		visited[it.first] = 1;
				}
				else if(visited[it.first] == 1)
				{
					st->push(it.first);
				}
	    	}
			ptr = &cur;
		}

		std::queue<value_type> *q;
		std::stack<value_type> *st;

		std::unordered_map<value_type, int8_t> visited;
		graph_visit type;
		value_type cur;
		pointer ptr;
		directed_graph<value_type, e_type> *owner;
	} iterator;

//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//

	iterator begin(v_type node, graph_visit type = graph_visit::BFS) 
	{ return iterator(type, &node, this); }

	iterator end() 
	{ return iterator(graph_visit::BFS, 0, this); }
private:
    class compare_edges
    {
    public:
		bool operator()(const std::pair<v_type, e_type>& l, 
						const std::pair<v_type, e_type>& r)
		{ return l.second < r.second; }
    
	};
    
	unsigned int e;
    std::unordered_map<v_type, std::list<std::pair<v_type, e_type>>> adj_map;
    const e_type POSITIVE_INFINITY = std::numeric_limits<e_type>::max();
};

#endif //WASTE_UNDIRECTED_GRAPH
