#ifndef WASTE_DATA_STRUCTURE
#define WASTE_DATA_STRUCTURE

#include <union_find.hpp>
#include <binary_heap.hpp>
#include <stack.hpp>
#include <directed_graph.hpp>
#include <undirected_graph.hpp>
#include <avl_tree.hpp>
#include <rb_tree.hpp>
#include <fenwick_tree.hpp>

#endif //WASTE_DATA_STRUCTURE
