#ifndef WASTE_AVL_TREE
#define WASTE_AVL_TREE

#include <iostream>
#include <string>
#include <sstream>
#include <cstddef>
#include <iterator>
#include <stack>

typedef enum
{
    INORDER,
    PREORDER,
    POSTORDER,
    LEVELORDER
} avl_visit;

template<typename K, typename V>
class avl_tree
{
    class avl_node
    {
    public:
		avl_node() : l(0), r(0) {}
		avl_node(avl_node *l, K key, V value, avl_node *r) 
		: key(key), value(value), l(l), r(r) { this->height = 1; }

		K key;
		V value;
		int height;
		avl_node *l;
		avl_node *r;
    };

public:
    
	avl_tree() { root = new avl_node; this->size = 0; }
    
	bool insert(K key, V value)
    {
		if(this->size == 0)
		{
		    this->root->key = key;
		    this->root->value = value;
		    this->size++;
		    return true;
		}
		else
		{
		    avl_node *node = insert_node(root, key, value);

		    if(node != root) return true;
		    else return false;
		}
    }

    const V search(K key)
    {
		avl_node* s = node_search(key);
		if(s->key == key) return s->value;
		else return 0;
    }

    void print(avl_visit t)
    {
        std::cout << "[";
        for(avl_tree<K, V>::iterator 
			it = this->begin(t); it != this->end(); it++)
	        std::cout << *it << ", ";
        std::cout << "\b\b]" << std::endl;
    }

    const std::string to_string_inorder(avl_node *node)
    {
		std::string s = "["; s.append(_inorder_to_string(node));
		s.append("\b\b]"); return s;
    }
    
	typedef struct iterator
    {
	    using iterator_category = std::forward_iterator_tag;
	    using difference_type   = std::ptrdiff_t;
	    using value_type        = V;
	    using pointer           = V*;
	    using reference         = V&;

	    iterator(avl_visit t, avl_node *node) : ptr(node), type(t) 
	    {
	    	switch(t)
            {
            	case avl_visit::PREORDER:
                st = new std::stack<avl_node*>();
	    	    pre_start(node);
	    	    break;

	    	    case avl_visit::INORDER:
	    	    st = new std::stack<avl_node*>();
                f = new std::stack<bool>();
                in_start(node);
	    	    break;

                case avl_visit::POSTORDER:
	    	    st = new std::stack<avl_node*>();
                f = new std::stack<bool>();
                post_start(node);
	    	    break;

                case avl_visit::LEVELORDER:
                inq = new std::queue<avl_node*>();
	    	    level_start(node);
                break;
	    	}
	    }
	    reference operator*() const { return ptr->value; }
	    pointer operator->() { return &(ptr->value); }

	    iterator& operator++() { step(); return *this; }
	    iterator operator++(int) { iterator tmp = *this; step(); return tmp; }

	    friend bool operator== (const iterator& a, const iterator& b) { return a.ptr == b.ptr; };
	    friend bool operator!= (const iterator& a, const iterator& b) { return a.ptr != b.ptr; };

    private:
        void step()
        {
            switch(this->type)
            {
            	case avl_visit::PREORDER:
	    	        pre_step();
                    break;
	    	    case avl_visit::INORDER:
                    in_step();
                    break;
                case avl_visit::POSTORDER:
                    post_step();
                    break;
                default:
	    	        level_step();
                    break;
	    	}
        }

	    void pre_start(avl_node *source) 
        {
            if(!source) return;
            st->push(source);
            pre_step();
        }
	
        void pre_step() 
        {
            if(st->empty()) { ptr = 0; return; }
            ptr = st->top();
            st->pop();
            if(ptr->r) st->push(ptr->r);
            if(ptr->l) st->push(ptr->l); 
        }
	
	    void in_start(avl_node *source) 
        {
            if(!source) return;
            st->push(source);
            f->push(false);
            in_step();
        }
	    
        void in_step() 
        {
            if(st->empty()) { ptr = 0; return; }
            while(f->top() == false)
            {
                ptr = st->top(); st->pop();
                f->pop();

                if (ptr->r) { st->push(ptr->r); f->push(false); }
		        st->push(ptr); f->push(true);
		        if (ptr->l) { st->push(ptr->l);  f->push(false);}
            }
            ptr = st->top(); st->pop();
            f->pop();
        }
	
	    void post_start(avl_node *source) 
        {
            if(!source) return;
            st->push(source);
            f->push(false);
            
            post_step();
        }
	    
        void post_step() 
        {
            if(st->empty()) { ptr = 0; return; }
            while(f->top() == false)
            {
                ptr = st->top(); st->pop();
                f->pop();

                st->push(ptr); f->push(true);
                if (ptr->r) { st->push(ptr->r); f->push(false); }
		        if (ptr->l) { st->push(ptr->l);  f->push(false); }
            }
            ptr = st->top(); st->pop();
            f->pop();
        }

	    void level_start(avl_node *source) 
        {
            if(!source) return;
            inq->push(source);

           level_step();
        }
	    
        void level_step() 
        {
            if(inq->empty()) { ptr = 0; return; }
            ptr = inq->front();
            inq->pop();
            if(ptr->r) inq->push(ptr->r);
            if(ptr->l) inq->push(ptr->l); 
        }
	    
        avl_node *ptr;
        avl_visit type;
	    std::queue<avl_node*> *inq;
	    std::stack<avl_node*> *st;
        std::stack<bool> *f;
    } iterator;

    iterator begin(avl_visit type) { return iterator(type, root); }
    iterator end() { return iterator(avl_visit::INORDER, 0); }
    

private:
    std::string _inorder_to_string(avl_node *node)
    {
	if(node != nullptr)
	{
	    std::string s;
	    s.append(_inorder_to_string(node->l));
	    std::stringstream ss;
	    ss << "(" << node->key << ", " << node->value << "), ";
	    s.append(ss.str());
	    s.append(_inorder_to_string(node->r));
	    std::cout << s << std::endl;
	    return s;
	}
	else return "";
    }

    avl_node *insert_node(avl_node *node, K key, V value)
    {

	if(node == 0) return new avl_node(0, key, value, 0);

	if(node->key < key) node->r = insert_node(node->r, key, value);
	else if(node->key > key) node->l = insert_node(node->l, key, value);
	else return node;
	node->height = 1 + std::max(height(node->l), height(node->r));

	int balance = get_balance(node);

	if(balance > 1 && key < node->l->key)
	    return right_rotate(node);

	if(balance < -1 && key > node->r->key)
	    return left_rotate(node);

	if(balance > 1 && key > node->l->key)
	{
	    node->l = left_rotate(node->l);
	    return right_rotate(node);
	}

	if(balance < -1 && key < node->r->key)
	{
	    node->r = right_rotate(node->r);
	    return left_rotate(node);
	}

	return node;
    }

    avl_node* node_search(K key)
    {
	avl_node *tmp = root;
	while(true)
	{
	    if(tmp->key == key || (!tmp->l && !tmp->r))
		return tmp;

	    if(tmp->key > key)
	    {
		if(tmp->l) tmp = tmp->l;
		else return tmp;
	    }
	    else
	    {
		if(tmp->r) tmp = tmp->r;
		else return tmp;
	    }
	}
    }

    avl_node *right_rotate(avl_node *y)
    {
	avl_node *x = y->l;
	avl_node *temp = x->r;
	if(x == root) root = y;
	
        x->r = y;
	y->l = temp;

	y->height = std::max(height(y->l), height(y->r)) + 1;
	x->height = std::max(height(x->l), height(x->r)) + 1;

	return x;
    }

    avl_node *left_rotate(avl_node *x)
    {
	avl_node *y = x->r;
	avl_node *temp = y->l;
	if(x == root) root = y;

	y->l = x;
	x->r = temp;

	x->height = std::max(height(x->l), height(x->r)) + 1;
	y->height = std::max(height(y->l), height(y->r)) + 1;
	return y;
    }

    int height(avl_node *node)
    {
	if(node == 0) return 0;
	return node->height;
    }

    int get_balance(avl_node *node)
    {
	if(node == 0) return 0;
	return height(node->l) - height(node->r);
    }

    avl_node *root;
    unsigned long size;
};

#endif //WASTE_AVL_TREE