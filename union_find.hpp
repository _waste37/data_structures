#ifndef WASTE_UNION_FIND
#define WASTE_UNION_FIND

#include <unordered_map>
#include <vector>

template <typename T>
class union_find
{
    std::unordered_map<T, T> parent;
    std::unordered_map<T, int> rank;
public:
    union_find() {}
    union_find(std::vector<T> U)
    {
	for(typename std::vector<T>::iterator
		it = U.begin(); it != U.end(); it++)
	    make_set(*it);
    }

    void make_set(T X)//O(1)
    {
	parent[X] = X;
	rank[X] = 1;
    }

    bool union_set(T X, T Y)// O(log n)
    {
	T x_set = find(X);
	T y_set = find(Y);
	if(x_set == y_set) return false;

	if(rank[x_set] < rank[y_set])
	{  parent[x_set] = y_set; }
	else
	{
	    parent[y_set] = x_set;
	    if(rank[y_set] == rank[x_set]) rank[x_set]++;
	}
	return true;
    }

    T find(T X)//O(log n)
    {
	T prev = X;
	if(prev != parent[prev])
	{
	    T tmp = parent[prev];
	    while(tmp != parent[tmp])
	    {
		parent[prev] = parent[tmp];
		prev = tmp;
		tmp = parent[tmp];
	    }
	    parent[X] = tmp;
	    prev = tmp;
	}
	return prev;
    }
};

#endif
